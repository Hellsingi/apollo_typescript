declare namespace NodeJS {
    export interface ProcessEnv {
        NODE_ENV: string;
        DB_USERNAME: string;
        DB_DATABASE: string;
        DB_PASSWORD: string;
    }
}