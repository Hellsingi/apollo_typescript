import Tools from './index';
import db from '../models';

const bcrypt = require('bcryptjs');

const fnWrapper = (f: any): any => {
    return function () {
        try {
            // return f.apply(this, arguments);
        } catch (e) {
            throw e;
        }
    }
}

const expectWrapper = fnWrapper(expect);


describe('test function helloWorld', () => {
    it('Testing function helloWorld from class Tools', () => {
        // const ss = Tools.helloWorld()

        expect(Tools.helloWorld()).toEqual(`👋 Hello world! 👋`);
    });
})

describe('Testing connection to database', () => {
    let status = '';
    it('connect to database', done => {

        db.authenticate()
            .then(() => {
                status = 'Ok';
                expect(status).toEqual('Ok');
                done();
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
                done(err);
            });
    });
});

// describe('Testing CRUD operation', () => {
//     const test = {
//         name: 'Ilya JEST',
//         email: 'ilya@mail.com',
//         password: '123',
//     };

//     it('Testing createUser functions', async done => {
//         Tools.createUser('_', test)
//             .then(user => {
//                 expect(user.get('name')).toEqual(test.name);
//                 expect(user.get('email')).toEqual(test.email);

//                 done();
//             })
//             .catch(err => done(err));
//     });

//     it('Testing updateUser functions', async done => {
//         const testUpdate = {
//             id: 9,
//             params: {
//                 name: 'testUpdate JEST',
//                 email: 'updateEmail@malala.ru',
//                 password: await bcrypt.hash('ssss', 10)
//             },
//         };

//         Tools.updateUser('_', { id: testUpdate.id, ...testUpdate.params })
//             .then(res => {
//                 expect(res).toEqual('success update');
//                 done();
//             })
//             .catch(err => done(err));
//     });


//     // it('Testing deleteUser functions', async done => {
//     //     const  idUserDelete = 5;
//     //     Tools.deleteUser('_', { id: idUserDelete })
//     //         .then(res => {
//     //             expect(res).toEqual('success delete');
//     //             done();
//     //         })
//     //         .catch(err => done(err));
//     // });

//     it('Testing findUser functions', async done => {
//         const id = { id: 10, }

//         const needUser = {
//             name: 'Ilya JEST',
//             email: 'ilya@mail.com',
//         }

//         Tools.findUserById('_', id)
//             .then(user => {
//                 if (user) {
//                     expect(user.get('name')).toEqual(needUser.name);
//                     expect(user.get('email')).toEqual(needUser.email);
//                     done();
//                 }
//             })
//             .catch(err => done(err));
//     });
// });


describe('Testing login functions', () => {
    const test = {
        name: 'resl',
        password: '123',
        email: 'er@mai.ei',
        id: 8
    };

    const notValidTest = {
        name: 'reslDD',
        password: '123DD',
    }

    it('Testing correct login functions', done => {
        Tools.login('_', { name: test.name, password: test.password })
            .then(user => {
                expect(user.user.get('id')).toEqual(test.id);
                expect(user.user.get('email')).toEqual(test.email);
                done();
            })
            .catch(err => done(err));
    });

    it('Testing uncorrect name login functions', async () => {
        Tools.login('_', { name: notValidTest.name, password: test.password })
        try {
            return false;
        } catch (err) {
            expect(err).toMatch('Error: Invalid username name');
        }
    });

    it('Testing uncorrect password login functions', async () => {
        Tools.login('_', { name: test.name, password: notValidTest.password })
        try {
            return false;
        } catch (err) {
            expect(err).toMatch('Error: Invalid password');
        }
    });

});