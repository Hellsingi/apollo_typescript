import Tools from './index';
import db from '../models';
const assert = require('assert');
const bcrypt = require('bcryptjs');


describe('test function helloWorld', () => {
    it('Testing function helloWorld from class Tools', () => {
        assert.equal(Tools.helloWorld(), `👋 Hello world! 👋`);
    });
});


describe('Testing connection to database', () => {
    let status = '';
    it('connect to database', done => {

        db.authenticate()
            .then(() => {
                status = 'Ok';
                assert.equal(status, `Ok`);
                done();
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
                done(err);
            });
    });
});


// describe('Testing CRUD operation', () => {
//     const test = {
//         name: 'Ilya MOCHA',
//         email: 'ilyaMOCHA@mail.com',
//         password: '1234'
//     };

//     // it('Testing createUser functions', done => {
//     //     Tools.createUser('_', test)
//     //         .then(user => {
//     //             assert.equal(user.get('name'), test.name);
//     //             assert.equal(user.get('email'), test.email);
//     //             done();
//     //         })
//     //         .catch(err => done(err));
//     // });



//     // it('Testing updateUser functions', done => {
//     //     const testUpdate = {
//     //         id: 1,
//     //         para: {
//     //             name: 'testUpdate Mocha',
//     //             email: 'updateEmail@mail.com',
//     //             password: 'eeeee'
//     //         },
//     //     };

//     //     Tools.updateUser('_', { id: testUpdate.id, ...testUpdate.para })
//     //         .then(res => {
//     //             assert.equal(res, 'success update');

//     //             done();
//     //         })
//     //         .catch(err => done(err));
//     // });

//     // it('Testing deleteUser functions', done => {
//     //     const idUserDelete = 33;
//     //     Tools.deleteUser('_', { id: idUserDelete })
//     //         .then(res => {
//     //             assert.equal(res, 'success delete');
//     //             done();
//     //         })
//     //         .catch(err => done(err));
//     // });

//     // it('Testing findUser functions', done => {
//     //     const id = { id: 10, }

//     //     const needUser = {
//     //         name: 'Vasya',
//     //         email: 'vasya@mail.ru',
//     //     }

//     //     Tools.findUserById('_', id)
//     //         .then(user => {
//     //             if (user) {
//     //                 assert.equal(user.get('name'), needUser.name);
//     //                 assert.equal(user.get('email'), needUser.email);
//     //                 done();
//     //             }

//     //         })
//     //         .catch(err => done(err));
//     // });

// });


describe('Testing login functions', () => {
    const test = {
        name: 'resl',
        password: '123',
        email: 'er@mai.ei',
        id: 8
    };

    const notValidTest = {
        name: 'reslDD',
        password: '123DD',
    }

    it('Testing correct login functions', done => {
        Tools.login('_', { name: test.name, password: test.password })
            .then(user => {
                assert.equal(user.user.get('id'), test.id);
                assert.equal(user.user.get('email'), test.email);
                done();

            })
            .catch(err => done(err));
    });

    it('Testing uncorrect name login functions', async () => {
        Tools.login('_', { name: notValidTest.name, password: test.password })
        try {
            return false;
        } catch (err) {
            expect(err).toMatch('Error: Invalid username name');
        }
    });

    it('Testing uncorrect password login functions', async () => {
        Tools.login('_', { name: test.name, password: notValidTest.password })
        try {
            return false;
        } catch (err) {
            expect(err).toMatch('Error: Invalid password');
        }
    });

});
