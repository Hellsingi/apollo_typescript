import db from '../models';
import Tools from './index';
import schema from '../schema';
import { expect } from 'chai';
import Faker from 'faker';
import gql from 'graphql-tag';


const { ApolloServer } = require('apollo-server');
const { createTestClient } = require('apollo-server-testing');
const server = new ApolloServer({ schema });

const { mutate, query } = createTestClient(server);


const fakeUser = {
    name: Faker.internet.userName(),
    email: Faker.internet.email(),
    password: Faker.internet.password()
}

const corectUser = {
    name: 'resl',
    password: '123',
    id: 8
}

const convert = (str: string) => JSON.stringify(str);


// const LOGIN = gql`
//   mutation login($name: String!, $password: String!) {
//     login(email: $email)
//   }
// `;


// The `listen` method launches a web server.
server.listen().then(({ url }: { url: any }) => {
    console.log(`🚀  Server ready at ${url}`);
});

describe("testing query functions", () => {
    it("query helloWorld", async () => {
        const result = await query({
            query: `query{
        helloWorld
      }`,
        });
        const compare = { helloWorld: `👋 Hello world! 👋` }
        expect(compare.helloWorld).to.equal(JSON.parse(JSON.stringify(result.data)).helloWorld)
    })

    it("query findUserById", async () => {
        const result = await query({
            query: `query{
        findUserById(id: ${corectUser.id}){
            name
        }
      }`,
        });
        expect(corectUser.name).to.equal(result.data.findUserById.name)
    })

    it("query findUsersWithName", async () => {
        const result = await query({
            query: `query{
            findUsersWithName(name: ${convert(corectUser.name)}){
            id
            email
        }
      }`,
        });
        expect(corectUser.id).to.equal(result.data.findUsersWithName[0].id)
    })
});


describe("testing Mutation functions", () => {
    it("mutation login", async () => {
        const result = await mutate({
            mutation: `mutation {
        login(name: ${convert(corectUser.name)}, password:${convert(corectUser.password)}){
            token
          }
        }`,
        });
        const { token } = JSON.parse(JSON.stringify(result.data)).login
        expect(token.length).to.not.equal(0)
    })

    // it("mutation createUser", async () => {
    //     const result = await mutate({
    //         mutation: `mutation {
    //         createUser(name: ${convert(fakeUser.name)}, email: ${convert(fakeUser.email)},password:${convert(fakeUser.password)})
    //             {
    //                 name
    //                 email
    //             }
    //         }`,
    //     });
    //     expect(fakeUser.name).to.equal(result.data.createUser.name);
    //     expect(fakeUser.email).to.equal(result.data.createUser.email);
    // })

    it("mutation updateUser", async () => {
        const result = await mutate({
            mutation: `mutation {
                updateUser(id: ${corectUser.id + 1},name: ${convert(fakeUser.name)}, email: ${convert(fakeUser.email)})
                
            }`,
        });
        const compare = { updateUser: `success update` };
        expect(compare.updateUser).to.equal(result.data.updateUser);
    })
});
