import db from '../models';
// import e = require('express');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')

const { User } = db.models;

// type ParamsType {
//     id: number,
//     email: string,
//     password: string
// }

export default class Tools {
    static helloWorld = (root: void): string => {
        return `👋 Hello world! 👋`;
    }

    static square = (x: number) => x * x;

    static createUser = async (root: any, { name, email, password }: { name: string, email: string, password: string }) => {
        const token = jwt.sign(
            {
                id: password,
                username: name,
            },
            'Ilya',
            {
                expiresIn: '30d', // token will expire in 30days
            },
        )

        return User.create({
            name: name,
            email: email,
            password: await bcrypt.hash(password, 10),
            token: token
        })
    }

    static deleteUser = async (root: any, { id }: { id: number }) => {
        return User.destroy(
            { where: { id } }
        )
            .then((res) => res ? 'success delete' : 'not delete')
            .catch((err) => console.error(err))
    }

    static updateUser = async (root: any, { id, ...params }: { id: number, name: string, email: string, password: string }) => {
        return User.update(
            { ...params },
            { where: { id } }
        )
            .then((res) => res[0] ? 'success update' : 'not update')
            .catch((err) => console.error(err))
    }

    static findUserById = async (root: any, { id }: { id: number }) => User.findOne({ where: { id } });

    static findUsersWithName = async (root: any, args: any) => User.findAll({ where: args });

    static login = async (root: any, { name, password }: { name: string, password: string }) => {
        const user: any = await User.findOne({ where: { name } })

        if (!user) {
            throw new Error('Invalid username name')
        }

        const passwordMatch = await bcrypt.compare(password, user.password);

        if (!passwordMatch) {
            throw new Error('Invalid password')
        }


        const token = jwt.sign(
            {
                id: password,
                username: name,
            },
            'Ilya',
            {
                expiresIn: '30d', // token will expire in 30days
            },
        )

        return {
            user,
            token
        }
    }

    static currentUser = async (parent: any, args: any, { user }: { user: any }) => {
        if (!user) {
            throw new Error('Not Authenticated');
        }
        return user;
    }

    static getUser = async (token: string) => {
        try {
            if (token) {
                const verify = jwt.verify(token, 'Ilya');
                const userId = verify.id;

                if (userId) {
                    const user = User.findOne({ where: { name: verify.username } });
                    if (user) {
                        return user
                    }
                }
            }
            throw new Error('You must be logged in');
        } catch (err) {
            throw new Error('You must be logged in');
        }
    }
}