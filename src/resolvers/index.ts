import { IResolvers } from 'graphql-tools';
import Tools from '../controller';
import db from '../models';

const bcrypt = require('bcryptjs');

const resolverMap: IResolvers = {
    Query: {
        helloWorld: () => Tools.helloWorld(),
        findUserById: async (root, { id }) => Tools.findUserById(root, { id }),
        findUsersWithName: async (root: any, args: any) => Tools.findUsersWithName(root, args),
        
        currentUser: async (parent, args, { user }) => Tools.currentUser(parent, args, { user })
    },
    Mutation: {
        createUser: async (root, { name, email, password }) => Tools.createUser(root, { name, email, password }),
        updateUser: async (root, { id, ...params }) => Tools.updateUser(root, { id, ...params }),
        deleteUser: async (root, { id }) => Tools.deleteUser(root, { id }),

        login: async (root, { name, password }) => Tools.login(root, { name, password }),

    },
};
export default resolverMap; 