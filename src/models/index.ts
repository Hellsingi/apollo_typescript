'use strict';

import { Sequelize, DataTypes } from 'sequelize';
import * as path from 'path';
import User from './user';
import Recipe from './recipe';
require('dotenv').config();
// const config: any = require(path.resolve(__dirname, '../../src/config/config'));
const config: any = require(path.resolve(process.cwd(), './src/config/config'));

const dbConfig: any = config[process.env.NODE_ENV];

const sequelize = new Sequelize(
    dbConfig['database'],
    dbConfig['username'],
    dbConfig['password'],
    dbConfig
);
User.init(
    {
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: true
        }
        ,
        token: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
    tableName: 'Users',
    sequelize
});


Recipe.init(
    {
        titile: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ingredients: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        direction: {
            type: DataTypes.TEXT,
            allowNull: false
        },
    }, {
    tableName: 'Resipes',
    sequelize: sequelize, // this bit is important
});

export default sequelize;