'use strict';
// module.exports = (sequelize, DataTypes) => {
//   const Recipe = sequelize.define('Recipe', {
//     title: {
//       type: DataTypes.STRING,
//       allowNull: false
//     },
//     ingredients: {
//       type: DataTypes.STRING,
//       allowNull: false
//     },
//     direction: {
//       type: DataTypes.STRING,
//       allowNull: false
//     }
//   }, {});
//   Recipe.associate = function (models) {
//     // associations can be defined here
//     Recipe.belongsTo(models.User, { foreignKey: 'userId' })
//   };
//   return Recipe;
// };

import { Model } from 'sequelize';

export default class Recipe extends Model {
  public title!: string;
  public ingredients!: string;
  public direction!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}