// src/index.js

// const { ApolloServer } = require('apollo-server')
// const typeDefs = require('./schema')
// const resolvers = require('./resolvers')
// const models = require('../models')

import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import depthLimit from 'graphql-depth-limit';
import { createServer } from 'http';
import compression from 'compression';
import cors from 'cors';
import models from './models';
import schema from './schema';

import Tools from './controller';

const jwt = require('jsonwebtoken');

const app = express();

// const getUser = (token: string) => {
//     try {
//         if (token) {
//             return jwt.verify(token, 'Ilya')
//         }
//         return null
//     } catch (err) {
//         return null
//     }
// }

const server = new ApolloServer({
    schema,
    validationRules: [depthLimit(7)],
    context: ({ req }) => {
        const token = req.headers.authorization || '';
        const user = Tools.getUser(token);

        try {
            return { user };
        } catch {
            throw new Error('You provide incorrect token');
        }
    }
})

// server
//     .listen()
//     .then(({ url }) => console.log('Server is running on localhost:4000'))

app.use('*', cors());
app.use(compression());
server.applyMiddleware({ app, path: '/graphql' });

const httpServer = createServer(app);

httpServer.listen(
    { port: 3000 },
    (): void => console.log(`\n🚀      GraphQL is now running on http://localhost:3000/graphql`)
)